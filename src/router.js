import { createRouter, createWebHistory } from 'vue-router'
import App from './App.vue'
import Home from './components/Home.vue'
import RutaCrear from './components/RutaCrear.vue'
import RutaConsultarModificar from './components/RutaConsultarModificar.vue'
import RutaRutas from './components/RutaRutas.vue'
import RolConsultar from './components/RolConsultar.vue'
import RolCrear from './components/RolCrear.vue'

const routes = [{
    path: '/',
    name: 'root',
    component: App
  },
  {
    path: '/home',
    name: "home",
    component: Home
  },
  {
    path: '/RutaCrear',
    name: "rutacrear",
    component: RutaCrear
  },
  {
    path: '/RutaConsultarModificar',
    name: "rutaconsultarmodificar",
    component: RutaConsultarModificar
  },
  {
    path: '/RutaRutas',
    name: "rutarutas",
    component: RutaRutas
  },
  {
    path: '/RolConsultar',
    name: "RolConsultar",
    component: RolConsultar
  },
  {
    path:'/RolCrear',
    name:"RolCrear",
    component:RolCrear
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;